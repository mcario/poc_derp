# POC-DERP

Prove of Concept of a Docker, Nginx, Redis, PHP stack

An Nginx supported by a PHP-FPM to interpret the php code (files), and the PHP saving sessions to a Redis.

You will find a Docker-compose file to start the stack in a test environment, and the yaml files to start in a Kubernetes environment.

![Team](/images/IMG_4479.JPG)
